$(document).ready(() => {
    $("#searchText").on('keyup', function (event) {
        event.preventDefault();
        getMovies($("#searchText").val());
    })
});

function getMovies(searchText) {
    $.ajax({
        url: 'http://www.omdbapi.com/?apikey=e09c0bfd&s=' + searchText,
        type: "GET",
    })
        .done((response) => {
            console.info(response);
            addMovies(response.Search);
        })
        .fail((response) => {
            console.error("failed:" + response);
        })
        .always((response) => {
            console.log("complete!");
        })
}

function addMovies(movies) {
    var output = "";
    $.each(movies, (index, movie) => {
        if(movie.Poster === "N/A"){
            movie.Poster = "http://via.placeholder.com/200?text=Image%20not%20found";
        }
        output += `
        <div class="col-md-3">
            <div class="card bg-light mb-3">
                <div class="card-header">
                    ${movie.Title}
                </div>
                
                <div class="card-body">
                        <img src="${movie.Poster}">
                        <span class="badge badge-secondary">Year:${movie.Year}</span>
                        <a href="#" onclick="saveMovie('${movie.imdbID}')" class="badge badge-primary">Movie Details</a>
                </div>
            </div>
        </div>
        `;
    });

    $("#movies").html(output);
}

function saveMovie(id) {
    sessionStorage.setItem('movieId', id);
    window.location = "movie.html";
}

function getMovie() {
    var id = sessionStorage.getItem('movieId');
    $.ajax({
        url: 'http://www.omdbapi.com/?apikey=e09c0bfd&i=' + id,
        type: "GET",
    })
        .done((response) => {
            console.info(response);
            addMovie(response);
        })
        .fail((response) => {
            console.error("failed:" + response);
        })
        .always((response) => {
            console.log("complete!");
        })
}

function addMovie(movie) {
    var output = "";
    if(movie.Poster === "N/A"){
        movie.Poster = "http://via.placeholder.com/300x450?text=Image%20not%20found";
    }
    output = `
        <div class="media ml-2 mr-2">
            <img class="mr-3" src="${movie.Poster}">
            <div class="media-body">
                <ul class="list-group">
                            <li class="list-group-item"><strong>Title:</strong>${movie.Title}</li>

                            <li class="list-group-item"><strong>IMDB Link:</strong>
                            
                            <a href="http://www.imdb.com/title/${movie.imdbID}" class="btn-sm btn-primary" target="_blank">IMDB</a>
                            
                            </li>
                            <li class="list-group-item"><strong>IMDB Rating:</strong>${movie.imdbRating}</li>
                            <li class="list-group-item"><strong>Year:</strong>${movie.Year}</li>
                            <li class="list-group-item"><strong>Runtime:</strong>${movie.Runtime}</li>
                            <li class="list-group-item"><strong>Director:</strong>${movie.Director}</li>
                            <li class="list-group-item"><strong>Actors:</strong>${movie.Actors}</li>
                            <li class="list-group-item"><strong>Box Office Collection:</strong>${movie.BoxOffice}</li>
                            <li class="list-group-item"><strong>Plot:</strong>${movie.Plot}</li>
                </ul>
            </div>
        </div>
        
    `;

    $("#movie").html(output);

}

//TODO: Fix this function and  use this to load images or show a placeholder if img load error or N/A
function getImage(url, altUrl){
    var tester = new Image();
    tester.src = url;
    tester.addEventListener('error', function(tester, altUrl){
        tester.src = altUrl;
    });
    return tester;
}